[].forEach.call(document.getElementsByClassName('bee_input'), function (el) {
  let hiddenInput = document.createElement('input'),
    mainInput = document.createElement('input'),
    bees = [];

  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', el.getAttribute('data-name'));

  mainInput.setAttribute('type', 'text');
  mainInput.classList.add('main-input');
  mainInput.addEventListener('input', function () {
    let enteredBees = mainInput.value.split(',');
    if (enteredBees.length > 1) {
      enteredBees.forEach(function (t) {
        let filteredBee = filterBee(t);
        if (filteredBee.length > 0)
          addBee(filteredBee);
      });
      mainInput.value = '';
    }
  });

  mainInput.addEventListener('keyup', function (e) {

  })
  mainInput.addEventListener('keydown', function (e) {
    let keyCode = e.which || e.keyCode;
    if (keyCode === 8 && mainInput.value.length === 0 && bees.length > 0) {
      removeBee(bees.length - 1);
    }
  });

  el.appendChild(mainInput);
  el.appendChild(hiddenInput);

  addBee('hello!');

  function addBee(text) {
    let bee = {
      text: text,
      element: document.createElement('span'),
    };

    bee.element.classList.add('bee');
    bee.element.textContent = bee.text;

    let closeBtn = document.createElement('span');
    closeBtn.classList.add('close');
    closeBtn.addEventListener('click', function () {
      removeBee(bees.indexOf(bee));
    });
    bee.element.appendChild(closeBtn);

    bees.push(bee);

    el.insertBefore(bee.element, mainInput);

    refreshBees();
  }

  function removeBee(index) {
    let bee = bees[index];
    bees.splice(index, 1);
    el.removeChild(bee.element);
    refreshBees();
  }

  function refreshBees() {
    let beesList = [];
    bees.forEach(function (t) {
      beesList.push(t.text);
    });
    hiddenInput.value = beesList.join(',');
  }

  function filterBee(bee) {
    return bee.replace(/[^\w -]/g, '').trim().replace(/\W+/g, '-');
  }
});
