<?php

class cadastrarController extends Controller{
  
  private $abelha;
  private $flor;
  private $mes;

  public function __construct(){
    $this->abelha = new Abelha();
    $this->flor = new Flor();
    $this->mes = new Mes();
  }
  public function index(){}

  public function abelha(){
    $dados = array(
      'css' => 'cadastrar',
      'title' => 'Cadastre abelhas',
      'main_title' => 'Cadastre abelhas'
    );
    if(isset($_POST) && !empty($_POST)){
      $nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
      $especie = filter_input(INPUT_POST, 'especie', FILTER_SANITIZE_STRING);

      if(empty($nome) || empty($especie)){
        $_SESSION['erro'] = 'Preencha todos os campos!';                
      }
      else{
        $this->abelha->insert(
          ['nome','especie'],
          [$nome, $especie]
        );

        $_SESSION['success'] = 'Abelha cadastrada com sucesso';
        header('Location:'.BASE_URL);
        exit;
      }
    }

    if(isset($_SESSION['erro'])){
      $dados['nome'] = $nome;
      $dados['especie'] = $especie;
    }

    $this->loadTemplate('cadastrar_abelha',$dados);
  }

  public function flor(){
    $dados = array(
      'css' => 'cadastrar',
      'title' => 'Cadastre flores',
      'main_title' => 'Cadastre flores'
    );

    $dados['meses'] = $this->mes->getMeses();
    $dados['abelhas'] = $this->abelha->getAbelhas();
    
    if(isset($_POST) && !empty($_POST)){
      
      $nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
      $especie = filter_input(INPUT_POST, 'especie', FILTER_SANITIZE_STRING);
      $descricao = filter_input(INPUT_POST, 'descricao', FILTER_SANITIZE_STRING);
      $meses = filter_input(INPUT_POST, 'meses', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
      $imagem = 'flower_icon.png';
      $abelhas_id = explode(',',filter_input(INPUT_POST, 'abelhas_id', FILTER_SANITIZE_STRING));

      if(!empty($_FILES['imagem']['tmp_name'])){
        $imagem = $this->salvaImagem($_FILES['imagem']);
        if(empty($imagem)){
          $_SESSION['erro'] = 'Tipo de arquivo inválido...';
          header("Location:".BASE_URL."cadastrar/flor");
          exit;
        }
      }
      
      $flor_id = $this->flor->insert(
        ['nome','especie','descricao','imagem'],
        [$nome, $especie, $descricao, $imagem]
      );
      
      
      if(isset($flor_id) && !empty($flor_id)){
        $this->flor->insertIntoFloresMeses($flor_id,$meses);
        $this->flor->insertIntoFloresAbelhas($flor_id,$abelhas_id);
      }
    }

    $this->loadTemplate('cadastrar_flor',$dados);
  }

  // public function validateMes($mes){
  //   return filter_input(INPUT_POST, )
  // }
  private function salvaImagem($imagem){
    $tipos = ['image/jpg','image/png','image/jpeg'];
    $nome = md5(time(). rand(0, 9999)).'.jpg';

    if(in_array($imagem['type'], $tipos)){
        move_uploaded_file($imagem['tmp_name'],'assets/img/flores/'.$nome);
        return $nome;
    }
    
    return '';
  }
}