<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0, user-scalable=no">
  <title><?=(isset($title) && !empty($title))? $title : ''?></title>
  <link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Serif:wght@700&family=Roboto:wght@400;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/fontawesome-all.min.css">
  <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/estilos.css">
  <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/estilos.css">
	<link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/star-rating-svg.css">
	<?php
		if(isset($css) && !empty($css)){
			echo '<link rel="stylesheet" type="text/css" href="'.BASE_URL.'assets/css/'.$css.'.css">';
		}
	?>
</head>
<body>
  <!-- <?php
		// if(isset($header_type) && !empty($header_type)){
		// 	require_once('header_'.$header_type.'.php');
		// }
  ?> -->
  <main>
    <?php
      $this->loadViewInTemplate($viewName,$viewData);
    ?>
  </main>
    <footer class="rodape">
			<script src="<?=BASE_URL?>assets/js/jquery-3.2.1.min.js"></script>
			<script src="<?=BASE_URL?>assets/js/jquery.star-rating-svg.min.js"></script>
			<script src="<?=BASE_URL?>assets/js/script.js"></script>
    </footer>
</body>
</html>