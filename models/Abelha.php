<?php

class Abelha extends Model{
  public function where($campos_array, $campos_condicao, $valores, $tabela = "abelhas") {
    $resultado = parent::where($campos_array, $campos_condicao, $valores, $tabela);
    return $resultado;
  }
  public function query($sql) {
    return parent::query($sql);
  }
  
  public function insert($campo, $valor_campo, $tabela = 'abelhas') {
    $id_abelha = parent::insert($campo, $valor_campo, $tabela); 
    
    return $id_abelha;
  }
  
  public function getAbelhas(){
    $abelhas = $this->all('abelhas');
    return $abelhas; 

  }
  
}